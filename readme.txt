Main class: com.elmar.gameofthree.player.Player

For Start just need run two copy of project:
one with parameter: -Dspring.profiles.active=player1
another with parameter: -Dspring.profiles.active=player2

Executing run.bat can do it.
Or just run next commands

mvn clean package
After it finish run next commands in separate consoles.
mvn spring-boot:run -Dspring.profiles.active=player1
mvn spring-boot:run -Dspring.profiles.active=player2

After ~20-30 seconds applicatoin should be up so possible to open next urls.
http://localhost:8081/swagger-ui.html
http://localhost:8082/swagger-ui.html

There will be swagger ui which allow to user contact with REST interface. Two endpoints is important to use.
Game Endpoint -> GET /game
This endpoint allows to get information about last game.

Game Endpoint -> POST /game
This endpoint allows to start new game(if players are not playing currently).






