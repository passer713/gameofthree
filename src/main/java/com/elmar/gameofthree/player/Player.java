package com.elmar.gameofthree.player;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.jms.annotation.EnableJms;

@SpringBootApplication
@EnableJms
public class Player {

    public static void main(String[] args) {
        SpringApplication.run(Player.class);
    }

}
