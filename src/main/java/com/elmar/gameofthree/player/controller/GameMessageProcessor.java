package com.elmar.gameofthree.player.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.List;

@Component
public class GameMessageProcessor {
    private Logger logger = LoggerFactory.getLogger(GameMessageProcessor.class);

    private String opponentUrl;
    private boolean isGameStarted = false;
    private List<String> gameLog = new ArrayList<>();

    //numbers to add to a value to get a value divisible by 3
    private static final int[] numberAdd = new int[]{0, -1, 1};

    private JmsTemplate jmsTemplate;

    public GameMessageProcessor(@Value("${opponent.port}") final int opponentPort,
                                @Autowired JmsTemplate jmsTemplate) {
        opponentUrl = String.format("http://localhost:%d/game/move/", opponentPort);
        this.jmsTemplate = jmsTemplate;
    }

    @JmsListener(destination = "game")
    public void receiveMessage(Integer value) {
        if (value == 1) {
            gameLog.add("Opponent send:" + value);
            gameLog.add("Game over. Oppponent won");
            isGameStarted=false;
            return;
        }
        try {
            int newValue = calculateValue(value);
            sendValue(newValue);
            if (!isGameStarted) {
                isGameStarted = true;
                gameLog.clear();
                gameLog.add("New game started with value:" + value);
                gameLog.add("My value is:" + newValue);
            }else{
                gameLog.add("Opponent send:" + value);
                gameLog.add("My value is:" + newValue);
            }
            if(newValue==1) {
                gameLog.add("Game over. I won");
                isGameStarted=false;
            }
        } catch (Exception e) {
            logger.error("Can't connect to server. Retry later.");
            jmsTemplate.convertAndSend("game", value);
        }
    }

    private void sendValue(int value) {
        RestTemplate restTemplate = new RestTemplate();
        ResponseEntity<String> response = restTemplate.exchange(opponentUrl + value, HttpMethod.PUT, null, String.class);
        if (response.getStatusCodeValue() != 200)
            throw new RuntimeException("Invalid server response");
    }

    private int calculateValue(int value) {
        return (value + numberAdd[value % 3]) / 3;
    }

    public boolean isGameStarted() {
        return isGameStarted;
    }

    public List<String> getGameLog() {
        return gameLog;
    }
}
