package com.elmar.gameofthree.player.endpoint;

import com.elmar.gameofthree.player.controller.GameMessageProcessor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Random;

@RestController
public class GameEndpoint {

    @Autowired
    private JmsTemplate jmsTemplate;

    @Autowired
    private GameMessageProcessor gameMessageProcessor;

    @PostMapping("/game")
    public String startGame() {
        if(gameMessageProcessor.isGameStarted()){
            return "Game already started. Wait to end please.";
        }
        // we just put request to queue and response, so client shouldn't wait until whole game over
        jmsTemplate.convertAndSend("game",new Random().nextInt(Integer.MAX_VALUE));
        return "ok";
    }

    @PutMapping("/game/move/{initValue}")
    public String move(@PathVariable(value = "initValue") Integer initValue) {
        // we just put request to queue and response, so client shouldn't wait until whole game over
        jmsTemplate.convertAndSend("game",initValue);
        return "ok";
    }

    @GetMapping("/game")
    public List<String> getLog(){
        return gameMessageProcessor.getGameLog();
    }

}
