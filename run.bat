call mvn clean package

start mvn spring-boot:run -Dspring.profiles.active=player1
start mvn spring-boot:run -Dspring.profiles.active=player2

ping 127.0.0.1 -n 20 > nul

start http://localhost:8081/swagger-ui.html
start http://localhost:8082/swagger-ui.html